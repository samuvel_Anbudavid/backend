const mongoose = require('mongoose')
const express = require('express')
var fs = require('fs');
var http = require('http');
var https = require('https');
const uri = require('./config/key').mongoURI
const port = 3000
const conn = express();
const cors = require('cors');
const bodyParser = require('body-parser')
//  var privateKey  = fs.readFileSync('./config/9b79812e0d1a4def.pem', 'utf8');
//  var certificate = fs.readFileSync('./config/9b79812e0d1a4def.crt', 'utf8');
//  var credentials = {key: privateKey, cert: certificate};


conn.use(express.static(__dirname + '/'));
conn.use(express.urlencoded({
    extended: true
}));

conn.use(cors());
conn.use(express.json())
conn.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin: *");
    res.header("Access-Control-Allow-Credentials: true");
    res.header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
    res.header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
    
    next();
});
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.connect(uri, { 
    dbName: "doc_db",
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify: false
})
mongoose.connection.once('open', function () {
    console.log('connection has been made');
}).on('error', function (error) {
    console.log('error is:', error);
});

conn.use(express.json())
const routes = require('./data/routes/route');
conn.use('/dr-backend', routes)

// var httpServer = http.createServer(conn);
// var httpsServer = https.createServer(credentials, conn);
// httpServer.listen(port, () => {
//     console.log("server starting on port : " + port)
//   });
//   httpsServer.listen(port, () => {
//     console.log("server starting1 on port : " + port)
//   });
//httpsServer.listen(8443);


conn.listen(port, function () {
    console.log(`server started on port ${port}`)
})