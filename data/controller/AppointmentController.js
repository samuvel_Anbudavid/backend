const appointment =require('../model/appoinment')

const create_slot=async function(req,res)
{

   
    let promise = new Promise(async function (resolve, reject) {
        try {
        
                let id=req.body.doc_id
               
                let appiontcheck=await appointment.findOne({doctor_id: id, slot_type:req.body.slots_type,date_slot:req.body.date_slot,});
           
              if(appiontcheck){
                  
                await appointment.updateOne({doctor_id: id ,slot_type:req.body.slots_type,date_slot:req.body.date_slot},{$push:{slots:req.body.slot}});
                  
                    resolve({ success: true, message: "Appoint update"})
                }
                else {
                    await appointment.create({
                        doctor_id: req.body.doc_id,
                        doctor_name:req.body.doc_name,
                        slot_type:req.body.slots_type,
                        date_slot:req.body.date_slot,
                        slots:req.body.slot,
                          })
                    reject({ success: false, message: "Appointment Registered" })
                }
           

           
        } catch (error) {
            reject({ success: false, message: "error occured while register", error: error })
            console.log(error);
        }
    });
    promise.then(function (data) {
        res.send({ success: data.success, message: data.message })

    }).catch(function (error) {
        res.send({ success: error ? error.success : false, message: error ? error.message : 'error occured', error: error })
    })
    
}
const get_slot=async function(req,res)
{

   
    let promise = new Promise(async function (resolve, reject) {
        try {
        
                let id=req.body.doc_id
               
                let appoint=await appointment.find({doctor_id: id, date_slot:req.body.date_slot});
           
            if(appoint)
                  {
                    resolve({ success: true, message: "get Apoint",data:appoint})
                  }
            else
            {
                reject({ success: false, message: "not Appointment" })
            }
               
                   
                
           

           
        } catch (error) {
            reject({ success: false, message: "error occured while register", error: error })
            console.log(error);
        }
    });
    promise.then(function (data) {
        res.send({ success: data.success, message: data.message,data:data.data})

    }).catch(function (error) {
        res.send({ success: error ? error.success : false, message: error ? error.message : 'error occured', error: error })
    })
    
}
module.exports={
    create_slot,
    get_slot
}
