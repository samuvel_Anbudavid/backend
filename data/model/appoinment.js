const mongoose = require('mongoose')
const Schema = mongoose.Schema


const appointschema = new Schema({
  
    doctor_id:
    {
        type:String

    },
   doctor_name:
    {
        type:String,
    },
    slot_type:
    {
        type:Number
    },
    slots:
    {
        type:Array
    },
    date_slot:{
        type:String
    },
    createdAt: {
        type: Date,
        default: Date.now,
        required: true
    }
    
});


module.exports = appointments = mongoose.model("appointments", appointschema)